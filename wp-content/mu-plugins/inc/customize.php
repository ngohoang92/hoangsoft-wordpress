<?php
function generator_remove_version() {
	return '';
}
add_filter('the_generator', 'generator_remove_version');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link'); // remove really simple discovery (RSD) link
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
remove_action('wp_head', 'feed_links', 2); // remove rss feed links (if you don't use rss)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 ); // remove shortlink
remove_action( 'welcome_panel', 'wp_welcome_panel' );
function hoangsoft_plugin_support(){
	add_theme_support( 'custom-logo', array('header-text' => array( 'site-title', 'site-description' )) );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'customize-selective-refresh-widgets' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'gallery', 'video' ) );
	add_theme_support( 'title-tag' );
	add_theme_support( 'responsive-embeds' );
	set_post_thumbnail_size( 360, 360 );
}
add_action( 'after_setup_theme', 'hoangsoft_plugin_support' );
function remove_dashboard_widgets(){
    //remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
	remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');
function remove_network_dashboard_widgets(){
    remove_meta_box('dashboard_primary', 'dashboard-network', 'side');
}
add_action('wp_network_dashboard_setup', 'remove_network_dashboard_widgets');
// unregister all widgets
function unregister_default_widgets() {
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Calendar');
	//unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	//unregister_widget('WP_Widget_Search');
	//unregister_widget('WP_Widget_Text');
	//unregister_widget('WP_Widget_Categories');
	//unregister_widget('WP_Widget_Recent_Posts');
	//unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	//unregister_widget('WP_Widget_Tag_Cloud');
	//unregister_widget('WP_Widget_Media_Audio');
	//unregister_widget('WP_Widget_Media_Image');
	//unregister_widget('WP_Widget_Media_Video');
	//unregister_widget('WP_Widget_Custom_HTML');
}
add_action('widgets_init', 'unregister_default_widgets', 11);
function hoangsoft_admin_bar_menu_remove() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wp-logo' );
	//$wp_admin_bar->remove_menu( 'comments' );
}
add_action( 'wp_before_admin_bar_render', 'hoangsoft_admin_bar_menu_remove' );
function change_footer_admin () {
	return 'Copyright &copy; ' . date('Y') . ' Hoangsoft, LLC';
}
add_filter('admin_footer_text', 'change_footer_admin', 9999);
remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
add_filter('widget_text','do_shortcode');