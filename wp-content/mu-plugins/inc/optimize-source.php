<?php
/**
* Move Script to Footer
**/
/*function hoangsoft_move_script_to_footer() {
	remove_action('wp_head', 'wp_print_scripts');
	remove_action('wp_head', 'wp_print_head_scripts', 9);
	remove_action('wp_head', 'wp_enqueue_scripts', 1);
	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'hoangsoft_move_script_to_footer', 20 );*/

function hoangsoft_remove_jquery_migrate( &$scripts) {
	if(!is_admin()) {
		$scripts->remove('jquery');
		$scripts->add('jquery', false, array( 'jquery-core' ), false);
	}
}
add_action( 'wp_default_scripts', 'hoangsoft_remove_jquery_migrate' );
function hoangsoft_js_async_attr($tag, $handle, $src) {
	if (is_admin() || true == strpos($tag, '/wp-includes/' ) && false == strpos($tag, '/jquery/ui/' ) ) {
		return $tag;
	}
	$scripts_to_exclude = array('jquery.js');
	foreach($scripts_to_exclude as $exclude){
		if( true == strpos($tag, $exclude) ){
			return $tag;
		}
	}
	$scripts_to_async = array('wp-embed.min.js', 'lazyload.js', 'googlesitekit');
	foreach($scripts_to_async as $async){
		if( true == strpos($tag, $async) ){
			return str_replace( ' src', ' async src', $tag );
		}
	}
	return str_replace( ' src', ' defer src', $tag );
}
//add_filter( 'script_loader_tag', 'hoangsoft_js_async_attr', 999, 3);
function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}
function hoangsoft_css_preload_attr($src) {
	if( !is_admin() && !isset($_POST['preload']) ){
		$style_list = [get_stylesheet() . '/style.css', get_stylesheet() . '/style.min.css', 'hoangsoft-main-style.css'];
		if( false === strposa($src, $style_list) ){
			$src = str_replace(" rel='stylesheet'", " rel='preload'", $src);
			$src = str_replace(" type='text/css'", " as=\"style\" onload=\"this.rel='stylesheet'\"", $src);
		}
	}
    return $src;
}
//add_filter('style_loader_tag', 'hoangsoft_css_preload_attr', 999, 3);
/**
* Remove query strings
*/
function hoangsoft_remove_script_version( $src ) {
	$parts = explode( '?ver', $src );
	return $parts[0]; 
} 
add_filter( 'script_loader_src', 'hoangsoft_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'hoangsoft_remove_script_version', 15, 1 );
/**
* Disable the emoji's
*/
function hoangsoft_disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'hoangsoft_disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'hoangsoft_disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'hoangsoft_disable_emojis' );
function hoangsoft_disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}
function hoangsoft_disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}
	return $urls;
}