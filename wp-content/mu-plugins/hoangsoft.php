<?php
/*
Plugin Name: Custom by Hoangsoft
Plugin URI:  https://hoangsoft.com
Description: Hoangsoft mu-plugins
Version:     0.5
Author:      Hoangsoft, LLC
Author URI:  https://hoangsoft.com
Text Domain: hoangsoft
Domain Path: /languages
*/
define( 'WP_DEFAULT_THEME', 'hoangsoft' );
include_once('inc/smtp.php');
include_once('contact-form/contact-form.php');
include_once('inc/customize.php');
include_once('inc/optimize-source.php');
include_once('inc/clone-page.php');
add_action('admin_head', 'remove_help_tab');
function remove_help_tab(){
	$screen = get_current_screen();
    $screen->remove_help_tabs();
}
function time_elapsed_string($datetime) {
    if (empty($datetime)) {
        return "No date provided";
    }
    $periods = array("giây", "phút", "giờ", "ngày", "tuần", "tháng", "năm", "thập kỷ");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    $now = time();
    $unix_date = strtotime($datetime);
    if (empty($unix_date)) {
        return "Bad date";
    }
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = "trước";
    } else {
        $difference = $unix_date - $now;
        $tense = "vừa xong";
    }
    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }
    $difference = round($difference);
    /*if ($difference != 1) {
        $periods[$j].= "s";
    }*/
    return "$difference $periods[$j] {$tense}";
}
function get_site_logo($size = 'thumbnail'){
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , $size );
	$html = '<img src="' . $image[0] . '" alt="' . get_bloginfo('name') . '">';
	return $html;
}
add_filter('transient_dirsize_cache',function(){
	return;
});
add_action('wp_head', 'hoangsoft_ajaxurl');
function hoangsoft_ajaxurl() {
   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           var siteurl = "' . home_url('/') . '";
         </script>';
}
add_action( 'admin_bar_menu', 'admin_bar_item', 500 );
function admin_bar_item ( WP_Admin_Bar $admin_bar ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	$admin_bar->add_menu( array(
		'id'    => 'notifications',
		'parent' => 'top-secondary',
		'group'  => null,
		'title' => '<span class="ab-icon uil uil-bell" aria-hidden="true"></span>',
		'href'  => admin_url('admin.php?page=custom-page'),
		'meta' => [
			'title' => __( 'Thông báo', 'hoangsoft' ), 
		]
	) );
}
function hoangsoft_login_logo() { 
?> 
<style type="text/css">
body.login div#login h1 a {
	background-image: url(<?php echo plugins_url('/assets/img/favicon.png', __FILE__ ); ?>);
	width: 100px;
	height: 100px;
	background-size: 100px;
}
body.login .button{
	background: #ffbb3c;
    border-color: #ffbb3c;
	color: #fff;
}
body.login .button:hover,
body.login .button:active{
	background: #ffcb6a;
	border-color: #ffcb6a;
	color: #333;
}
</style>
<?php 
} 
add_action( 'login_enqueue_scripts', 'hoangsoft_login_logo' );
function hoangsoft_admin_theme_style() { 
	wp_enqueue_style('hoangsoft-admin-theme', plugins_url('/assets/css/admin.css', __FILE__));
?> 
<style type="text/css">
#wpadminbar .quicklinks li .blavatar{display: none !important;}
</style>
<?php 
}
add_action( 'admin_enqueue_scripts', 'hoangsoft_admin_theme_style' );
function hoangsoft_frontend_theme_style() { 
	wp_enqueue_style('hoangsoft-admin-theme', plugins_url('/assets/css/hoangsoft.css', __FILE__));
?> 
<style type="text/css">
#wpadminbar .quicklinks li .blavatar{display: none !important;}
</style>
<?php 
}
add_action( 'wp_head', 'hoangsoft_frontend_theme_style' );
// Function to allow .csv uploads
function hoangsoft_upload_mimes($mimes = array()) {
	// Add a key and value for the CSV file type
	$mimes['csv'] = "text/csv";
	return $mimes;
}
add_filter('upload_mimes', 'hoangsoft_upload_mimes');
function add_svg_to_upload_mimes( $upload_mimes ) { 
	$upload_mimes['svg'] = 'image/svg+xml'; 
	$upload_mimes['svgz'] = 'image/svg+xml'; 
	return $upload_mimes; 
} 
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );
function hoangsoft_plugin_widgets_init() {
	register_sidebar( array(
        'name'          => __( 'Custom HTML', 'hoangsoft' ),
        'id'            => 'custom-html',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'hoangsoft' ),
        'before_widget' => '<div id="%1$s" class="custom-html %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'hoangsoft_plugin_widgets_init' );
function hoangsoft_plugin_add_footer(){
	if ( is_active_sidebar( 'custom-html' ) ){
		dynamic_sidebar( 'custom-html' );
	}
}
add_action( 'wp_footer', 'hoangsoft_plugin_add_footer' );
// Apply filter
add_filter( 'get_avatar' , 'my_custom_avatar' , 1 , 5 );

function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;

    if ( is_numeric( $id_or_email ) ) {

        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );	
    }

    if ( $user && is_object( $user ) ) {

        if ( $user->data->ID == '1' ) {
            $avatar = site_url('/wp-content/mu-plugins/assets/img/d-avatar.png');
            $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
        }

    }

    return $avatar;
}