<?php
/**
 * Twenty Twenty-Two functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Hoangsoft
 * @subpackage Twenty_Twenty_Two
 * @since Twenty Twenty-Two 1.0
 */

if ( ! function_exists( 'hoangsoft_register_nav_menu' ) ) {
 
    function hoangsoft_register_nav_menu(){
        register_nav_menus( array(
            'primary_menu' => __( 'Primary Menu', 'hoangsoft' ),
            'footer_menu'  => __( 'Footer Menu', 'hoangsoft' ),
        ) );
    }
    add_action( 'after_setup_theme', 'hoangsoft_register_nav_menu', 0 );
}


if ( ! function_exists( 'hoangsoft_support' ) ) :

	/**
	 * Sets up theme defaults and registers support for various Hoangsoft features.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function hoangsoft_support() {

		// Add support for block styles.
		add_theme_support( 'wp-block-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style.css' );

	}

endif;

add_action( 'after_setup_theme', 'hoangsoft_support' );

if ( ! function_exists( 'hoangsoft_styles' ) ) :

	/**
	 * Enqueue styles.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function hoangsoft_styles() {
		// Register theme stylesheet.
		$theme_version = wp_get_theme()->get( 'Version' );

		$version_string = is_string( $theme_version ) ? $theme_version : false;
		wp_register_style(
			'hoangsoft-style',
			get_template_directory_uri() . '/style.css',
			array(),
			$version_string
		);

		// Enqueue theme stylesheet.
		wp_enqueue_style( 'hoangsoft-style' );
		
		/*wp_register_style(
			'bootstrap-style',
			get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css',
			array(),
			$version_string
		);

		// Enqueue theme stylesheet.
		wp_enqueue_style( 'bootstrap-style' );
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array(), $version_string, true );*/
	}

endif;

add_action( 'wp_enqueue_scripts', 'hoangsoft_styles' );

// Add block patterns
require get_template_directory() . '/inc/block-patterns.php';
